var locomotive = require('locomotive')
    , Controller = locomotive.Controller;

var querystring = require("querystring");
var request = require('request'); 

var spotifyClientId = "209e698f81cb41169440396af1069364";
var spotifyClientSecret = "6e0066d1ae054ff1bb12bec5bdb5b70a"
var spotifyCallbackUrl = "http://localhost:3000/spotify/callback"

var spotifyController = new Controller();
var stateKey = 'spotify_auth_state';

/**
 * Generates a random string containing numbers and letters
 * @param  {number} length The length of the string
 * @return {string} The generated string
 */
var generateRandomString = function(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

spotifyController.login = function() {
    var state = generateRandomString(16);
    this.res.cookie(stateKey, state);

    // your application requests authorization
    var scope = 'user-follow-read';
    this.redirect('https://accounts.spotify.com/authorize?' +
        querystring.stringify({
        response_type: 'code',
        client_id: spotifyClientId,
        scope: scope,
        redirect_uri: spotifyCallbackUrl,
        state: state
        }));
}

spotifyController.callback = function() {
     // your application requests refresh and access tokens
  // after checking the state parameter

  var code = this.req.query.code || null;
  var state = this.req.query.state || null;
  var self = this;
  
  var storedState = this.req.cookies ? this.req.cookies[stateKey] : null;

  if (state === null || state !== storedState) {
    this.res.redirect('/#' +
      querystring.stringify({
        error: 'state_mismatch'
      }));
  } else {
    this.res.clearCookie(stateKey);
    var authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      form: {
        code: code,
        redirect_uri: spotifyCallbackUrl,
        grant_type: 'authorization_code'
      },
      headers: {
        'Authorization': 'Basic ' + (new Buffer(spotifyClientId + ':' + spotifyClientSecret).toString('base64'))
      },
      json: true
    };

    request.post(authOptions, function(error, response, body) {
      if (!error && response.statusCode === 200) {

        var access_token = body.access_token,
            refresh_token = body.refresh_token;

        var options = {
          url: 'https://api.spotify.com/v1/me',
          headers: { 'Authorization': 'Bearer ' + access_token },
          json: true
        };

        // use the access token to access the Spotify Web API
        request.get(options, function(error, response, body) {
          console.log(body);
        });

        // we can also pass the token to the browser to make requests from there
        self.res.redirect('/#' +
          querystring.stringify({
            access_token: access_token,
            refresh_token: refresh_token
          }));
      } else {
        self.res.redirect('/#' +
          querystring.stringify({
            error: 'invalid_token'
          }));
      }
    });
  }            
}

spotifyController.refreshToken = function() {
    var refresh_token = this.req.query.refresh_token;
  var authOptions = {
    url: 'https://accounts.spotify.com/api/token',
    headers: { 'Authorization': 'Basic ' + (new Buffer(spotifyClientId + ':' + spotifyClientSecret).toString('base64')) },
    form: {
      grant_type: 'refresh_token',
      refresh_token: refresh_token
    },
    json: true
  };

  request.post(authOptions, function(error, response, body) {
    if (!error && response.statusCode === 200) {
      var access_token = body.access_token;
      this.res.send({
        'access_token': access_token
      });
    }
  });
}
    
module.exports = spotifyController;