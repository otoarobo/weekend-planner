var locomotive = require('locomotive')
    , Controller = locomotive.Controller;

var parseString = require('xml2js').parseString;
const StringDecoder = require('string_decoder').StringDecoder;
var pagesController = new Controller();
var async = require("async");
var schemas = require("../../config/schemas");
var util = require("util");
var moment = require("moment");
var querystring = require("querystring");

var eventfulApiKey = "779HZnHHLXHKNmqd";
var weatherApiPrefix = "http://api.openweathermap.org/data/2.5";
var weatherApiKey = "c9eaacb531cadadaa2d994b5d81da4b0";
var Client = require('node-rest-client').Client;
var spotifyAccessToken = "BQCJ2rg8qGzmEoIqJD_6HUnnESsndcB9MUyAzPI-gbdO2Cw8uPRNzzJyG7RlHMXL5rfTlI_KZKfItDILs7eylwLyVAn0bAS5K9hNa5D9wogqVoq_wp_B-aWJpGVM1018ZAhR4o_doWbP8xjKG83D8qP4bMXdg1EnsrWkDwrGTWE";
var expediaApiKey = "AIV0q8dRgb2Ovz0HhTuUiRNytbzuu9cY";

var uberApiKey = "XFOJVUCW92NLWUtAWwjw652C_yeLlKhX"
var uberMobileSite = "https://m.uber.com/sign-up?";

function tryCache(url, params, callback) {
    var client = new Client();
    console.log("Looking for cached data: " + url);
    
    schemas.CacheSchema.findOne({ url: url }, function(err, result) {
        if (result == null) {
            console.log("No cache hit, performing request");
            client.get(url, params, function(data, response) {
                console.log("Saving data to cache ...");
                
                new schemas.CacheSchema({
                    url: url,
                    data: JSON.stringify(data)
                }).save(function(err2, newData) {
                    console.log(err2);
                    
                    callback(err2, data);
                });
            });
        } else {
            callback(err, JSON.parse(result.data));
        }
    });
}

function padDigits(number, digits) {
    return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}

function getFandangoPreferences(forDate, userProfile, callback) {
    var client = new Client();
    async.waterfall([
        function (getFandangoPreferencesCallback) {
            console.log("Getting Fandango preferences");
            schemas.PreferencesSchema.find({
                type: "movie",
                service: "fandango"
            }, getFandangoPreferencesCallback);
        },
        function (fandangoResults, getMoviesCallback) {
            async.concatSeries(fandangoResults, function(fandango, getFandangoMoviesCallback) {                
                var url = util.format("http://www.fandango.com/rss/moviesnearme_%s.rss", userProfile.zipCode);
            
                client.get(url, function(buffer, response) {
                    const decoder = new StringDecoder('utf8');
                    var xml = decoder.write(buffer);                    
                    
                    parseString(xml, function (err, result) {
                        var items = result.rss.channel[0].item;
                        for(var i = 0; i < items.length; i++) {
                            var item = items[i];
                            var pubDate = item.pubDate[0];
                            var date = moment(pubDate.toString(), "ddd, DD MMM YYYY hh:mm:ss Z");
                            var movies = [];
                            
                            console.log(date.format());
                            
                            if (date.dayOfYear() == forDate.dayOfYear()) {                                
                                movies.push({
                                    date: date.utc().format("LT"),
                                    title: item.title,
                                    link: item.link,
                                    description: item.description,
                                    source: fandango,
                                    importance: fandango.importance
                                });  
                            }
                        }
                        
                        console.log(movies);
                        getFandangoMoviesCallback(null, movies);
                        
                    });
                    
                   
                }); 
            }, getMoviesCallback);  
        }
    ], callback);
}

function getExpediaDrivePreferences(forDate, userProfile, callback) {
    var client = new Client();
    var dayAfter = moment(forDate).add(1, "days");
    
    if (forDate.day() != 0) {
        callback(null);
    } else {    
        async.waterfall([
            function (getExpediaDrivePreferencesCallback) {
                console.log("Getting Expedia Drive preferences");
                schemas.PreferencesSchema.find({
                    type: "stay",
                    service: "expedia"
                }, getExpediaDrivePreferencesCallback);
            },
            function (expediaResults, getHotelsCallback) {
                async.concat(expediaResults, function(expedia, getExpediaHotelsCallback) {
                    var params = querystring.stringify({
                        location: expedia.geo,
                        radius: "5km",
                        dates: util.format("%s,%s", forDate.format("YYYY-MM-DD"), dayAfter.format("YYYY-MM-DD")),
                        apikey: expediaApiKey});
                    
                    var url = util.format("http://terminal2.expedia.com/x/hotels?%s", params);
                
                    tryCache(url, undefined, function(err, data) {
                        var list = data.HotelInfoList.HotelInfo;
                        var hotels = [];
                        
                        for(var i=0; i < list.length; i++) {
                            var hotel = list[i];
                            var price = hotel.Price ? Number(hotel.Price.TotalRate.Value) : NaN;
                            var guestRating = Number(hotel.GuestRating);
                            
                            if (hotel.StatusCode == "0" &&
                                !isNaN(price) &&
                                guestRating >= expedia.minRating &&
                                price < userProfile.maxPrice) {
                                var rating = expedia.minRating - Number(hotel.StarRating);
                                hotels.push({
                                    starRating: hotel.StarRating,
                                    guestRating: guestRating,
                                    name: hotel.Name,
                                    location: hotel.Location.StreetAddress,
                                    description: hotel.Description,
                                    importance: expedia.importance + rating,
                                    price: price,
                                    link: hotel.DetailsUrl,
                                    source: expedia
                                })
                            }
                        }
                        
                        var result = null;
                        if (hotels.length > 0) {
                            result = {
                                hotels: hotels,
                                image: "",
                                source: expedia
                            }
                        }
                        
                        getExpediaHotelsCallback(null, result);
                    });
                }, getHotelsCallback);
                
                
            }
        ], callback);
    }
}

function getExpediaFlyPreferences(forDate, userProfile, callback) {
    var client = new Client();
    var dayAfter = moment(forDate).add(1, "days");
    
    if (forDate.day() != 0) {
        callback(null);
    } else {    
        async.waterfall([
            function (getExpediaFlyPreferencesCallback) {
                console.log("Getting Expedia Flight preferences");
                schemas.PreferencesSchema.find({
                    type: "fly",
                    service: "expedia"
                }, getExpediaFlyPreferencesCallback);
            },
            function (expediaResults, getFlightsCallback) {
                async.concat(expediaResults, function(expedia, getExpediaFlightsCallback) {
                    var params = querystring.stringify({
                        arrivalAirport: expedia.airportCode,
                        departureAirport: userProfile.airportCode,
                        departureDate: forDate.format("YYYY-MM-DD"),
                        returnDate: dayAfter.format("YYYY-MM-DD"),
                        apikey: expediaApiKey});
                    
                    var url = util.format("http://terminal2.expedia.com/x/mflights/search?%s", params);
                
                    client.get(url, function(data, response) {
                    //tryCache(url, undefined, function(err, data) {
                        var list = data.offers || [];
                        var flights = [];
                        
                        for(var i=0; i < list.length; i++) {
                            var flight = list[i];
                            var price = flight.totalFare ? Number(flight.totalFare) : NaN;
                            var legs = flight.legIds.length;
                            
                            if (!isNaN(price) &&
                                legs <= expedia.maxLegs &&
                                price < userProfile.maxPrice) {

                                var segments = [];
                                
                                for (var j=0; j < data.legs.length; j++) {
                                    for (var k=0; k < flight.legIds.length; k++) {
                                        if (data.legs[j].legId == flight.legIds[k]) {
                                            var leg = data.legs[j];
                                            var seg = leg.segments[0]; 
                                            var date = moment(seg.departureTime, "MMM D, YYYY h:m:s A")
                                            
                                            segments.push({
                                                time: date.format("hh:mm"),
                                                airlineCode: seg.airlineCode,
                                                flightNumber: seg.flightNumber
                                            });
                                        }
                                    }
                                }

                                flights.push({
                                    price: price,
                                    link: flight.detailsUrl,
                                    segments: segments,
                                    source: expedia
                                })
                            }
                        }
                        
                        var result = null;
                        if (flights.length > 0) {
                            result = {
                                flights: flights,
                                image: "",
                                source: expedia
                            }
                        }
                        
                        getExpediaFlightsCallback(null, result);
                    });
                }, getFlightsCallback);
                
                
            }
        ], callback);
    }
}

function getNHLGamePreferences(forDate, userProfile, callback) {
    var client = new Client();
    async.waterfall([
        function (getNHLGamePreferencesCallback) {
            console.log("Getting NHL game preferences");
            schemas.PreferencesSchema.findOne({
                type: "game",
                sport: "hockey"
            }, getNHLGamePreferencesCallback);
        },
        function (nhlResult, getNHLCalendarCallback) {
            var url = util.format("http://nhlwc.cdnak.neulion.com/fs1/nhl/league/clubschedule/%s/%s/iphone/clubschedule.json",
                      nhlResult.team, forDate.format("YYYY/MM"));
            
            tryCache(url, undefined, function(err, data) {
                getNHLCalendarCallback(err, nhlResult, data);
            });
        },        
        function (nhlResult, calendar, getGamesCallback) {
            var results = undefined;
            var games = [];
            
            for (var j = 0; j < calendar.games.length; j++) {
                var game = calendar.games[j];
                var date = moment(game.startTime, "YYYY/MM/DD hh:mm:ss");                                                       
                
                if (game.loc == "home" &&
                    date.dayOfYear() == forDate.dayOfYear()) {                                                      
                        
                    console.log(game);
                    games.push({
                        startTime: date.utc().format("hh:mm"),
                        versus: game.abb
                    });
                }                    
            }
            
            if (games.length > 0) {
                results = {
                    games: games,
                    image: "",
                    source: nhlResult,
                    importance: nhlResult.importance
                };
            }
            
            getGamesCallback(null, results);
        }
    ], callback);
}


function getSongKickPreferences(forDate, userProfile, callback) {
    var client = new Client();
     async.waterfall([
        function (getMusicPreferencesCallback) {
            console.log("Getting Songkick preferences");
            schemas.PreferencesSchema.findOne({
                type: "audioStreaming",
                service: "songkick"
            }, getMusicPreferencesCallback);
        },
        function(songkick, getFromSongKickCallback) {
            if (songkick == null) {
                getFromSongKickCallback(null, {});
            } else {
                var url = util.format("http://acousti.co/feeds/upcoming/%s", songkick.username);
                
                client.get(url, function(buffer, response) {
                    const decoder = new StringDecoder('utf8');
                    var xml = decoder.write(buffer);                    
                    
                    parseString(xml, function (err, result) {
                        var items = result.rss.channel[0].item;
                        for(var i = 0; i < items.length; i++) {
                            var item = items[i];
                            var pubDate = item.pubDate[0];
                            var date = moment(pubDate.toString(), "ddd, DD MMM YYYY hh:mm:ss Z");
                            var artists = [];                           
                            
                            if (date.dayOfYear() == forDate.dayOfYear()) {
                                 var uberParams = querystring.stringify({
                                    clientId: uberApiKey,
                                    dropoff_latitude: item["geo:lat"],
                                    dropoff_longitude: item["geo:long"]
                                 });
                                 
                                 console.log(item);
                                
                                artists.push({
                                    date: date.utc().format("LT"),
                                    title: item.title,
                                    link: item.link,
                                    description: item.description,
                                    source: songkick,
                                    image: "/images/joesatriani2.jpg",
                                    importance: songkick.importance,
                                    uberLink: uberMobileSite + uberParams
                                });  
                            }
                            
                            getFromSongKickCallback(null, artists);
                        }                        
                    });
                }); 
            }
        }
     ], callback);
}

function getSpotifyPreferences(forDate, userProfile, callback) {
    // function (preferences, getArtistsCallback) {            
        //     if (preferences == null) {
        //         getArtistsCallback(null);
        //     } else {
        //         tryCache("https://api.spotify.com/v1/me/following?type=artist&limit=50", {
        //             headers: {"Authorization": "Bearer " + spotifyAccessToken }
        //         }, function(err, data) {
        //             var items = data.artists.items;
        //             var artists = [];
        //             
        //             for(var i=0; i < items.length; i++) {
        //                 artists.push(items[i].name);
        //             }
        //             
        //             getArtistsCallback(null, artists);
        //         })
        //     }
        // },
        /*function(artists, getFromEventfulCallback) {
            var url = util.format("http://api.eventful.com/json/events/search?location=%s&date=%s%s%s&app_key=%s",
                userProfile.zipCode, padDigits(moment().year(), 4), padDigits(moment().month(), 2), padDigits(moment().day(), 2), eventfulApiKey);
                
            console.log(url);
            client.get(url, function(buffer, response) {
                const decoder = new StringDecoder('utf8');
                var data = JSON.parse(decoder.write(buffer));
                
                for (var i=0; i < data.events.event.length; i++) {
                    var event = data.events.event[i];    
                    
                    console.log(event);
                }
                
                getFromEventfulCallback(null); 
            });
        }, */
}

function getPlacePreferences(forDate, userProfile, callback) {
    async.waterfall([
        function (getPlacePreferencesCallback) {
            schemas.PreferencesSchema.find({
                type: "place"
            }, getPlacePreferencesCallback);
        },
        function (preferences, getWeatherPreferencesCallback) {
            if (preferences.length == 0) {
                getWeatherPreferencesCallback(null);
            } else {                
                async.concatSeries(preferences, function(placePreference, cb) {
                    var url = util.format("%s/forecast?units=imperial&zip=%s,us&APPID=%s", weatherApiPrefix, placePreference.zipCode, weatherApiKey);

                    tryCache(url, undefined, function (err, data) {
                        var results = [];
                        var samples = [];
                        
                        for (var i = 0; i < data.list.length; i++) {
                            var item = data.list[i];
                            
                            var date = moment.unix(item.dt);                            
                            
                            if (date.dayOfYear() == forDate.dayOfYear() &&
                                date.hour() >= 9 && date.hour() < 18) {
                                
                                samples.push({
                                    temperature: item.main.temp,
                                    icon: item.weather[0].icon,
                                    image: util.format("http://openweathermap.org/img/w/%s.png", item.weather[0].icon),
                                    main: item.weather[0].main,
                                    description: item.weather[0].description,
                                    hour: date.hour()                             
                                });  
                            }
                        }
                        
                        var avgTemperature = 0, isRaining = false;
                        if (samples.length > 0) {                            
                            samples.forEach(function(value) { 
                                avgTemperature += value.temperature;
                                isRaining = value.description.indexOf("rain") >= 0 ? true : isRaining; 
                             });
                            
                            avgTemperature /= samples.length;
                            console.log(util.format("Average temperature:  %d", avgTemperature));
                            console.log(util.format("Is raining:  %s", isRaining));
                            
                            var importance = placePreference.importance 
                            console.log(importance)
                            importance /= isRaining ? 2 : 1;
                            console.log("isRaining: " + importance)
                            
                            var lowTemp = avgTemperature - 50; 
                            importance += lowTemp / 100;
                            
                            results.push({
                                source: placePreference,
                                image: "",
                                samples: samples,
                                isRaining: isRaining,
                                importance: importance
                            });
                        } 
                        
                        cb(null, results);
                    }); 
                }, getWeatherPreferencesCallback);
            }
        }
    ], callback);
}

function buildRecommendations(forDate, userProfile, callback) {
    async.concatSeries([
        //getFandangoPreferences,
        getNHLGamePreferences,
        getExpediaDrivePreferences,
        getExpediaFlyPreferences,
        getSongKickPreferences,
        getPlacePreferences
    ], function(item, cb) {
        item(forDate, userProfile, cb);
    }, function (err, result) {
        
        console.log(result);
        
        var sorted = result.sort(function(a, b) {
            return b.importance - a.importance;
        });
        
        callback(err, sorted);
    });
}


pagesController.main = function () {
    var self = this;
    var client = new Client();

    async.waterfall([
        function (getProfileCallback) {
            schemas.UserProfile.findOne(getProfileCallback);
        },
        function (profile, getPreferencesCallback) {
            console.log(profile);
            
            var result = {
                user: {
                    name: profile.name,
                    profileImage: "/images/robert.jpg",
                    background: "/images/94105.jpg",
                    maxPrice: profile.maxPrice
                },
                days: []
            };

            var url = util.format("%s/weather?units=imperial&zip=%s,us&APPID=%s", weatherApiPrefix, profile.zipCode, weatherApiKey);
            client.get(url, function (data, response) {
                
                result.weather = {
                    temperature: data.main.temp,
                    icon: data.weather[0].icon,
                    image: util.format("http://openweathermap.org/img/w/%s.png", data.weather[0].icon),
                    main: data.weather[0].main,
                    description: data.weather[0].description
                };

                result.user.location = data.name;
                getPreferencesCallback(null, profile, result);
            });
        },
        function (userProfile, result, getSettingsCallback) {
            schemas.PreferencesSchema.find(function (err, preferences) {
                result.preferences = preferences;
                getSettingsCallback(err, userProfile, result);
            });
        },
        function (userProfile, result, getSaturdayRecommendations) {
            var date = moment().add(0, "days");
            buildRecommendations(date, userProfile, function(err, activities) {              
                result.days.push({
                    date: date.unix(),
                    activities: activities 
                });
                getSaturdayRecommendations(err, userProfile, result);
            });
        },
        function (userProfile, result, getSundayRecommendation) {
            var date = moment().add(1, "days");
            buildRecommendations(date, userProfile, function(err, activities) {              
                result.days.push({
                    date: date.unix(),
                    activities: activities 
                });
                getSundayRecommendation(err, userProfile, result);
            });
        },
        function (userProfile, result, getSaturdayRecommendations) {
            var date = moment().add(7, "days");
            buildRecommendations(date, userProfile, function(err, activities) {              
                result.days.push({
                    date: date.unix(),
                    activities: activities 
                });
                getSaturdayRecommendations(err, userProfile, result);
            });
        },
        function (userProfile, result, getSundayRecommendation) {
            var date = moment().add(8, "days");
            buildRecommendations(date, userProfile, function(err, activities) {              
                result.days.push({
                    date: date.unix(),
                    activities: activities 
                });
                getSundayRecommendation(err, result);
            });
        }

    ], function (err, result) {
        console.log(result.activities);
        
        self.user = result.user;
        self.days = result.days;
        
        var totalActivities = 0;
        for (var i=0; i < result.days.length; i++) {
            totalActivities += result.days[i].activities.length;
        }
        
        self.totalActivities = totalActivities
        self.weather = result.weather;
        self.preferences = result.preferences;
        self.render();
    });
}

pagesController.updateProfile = function () {   
    var body = this.req.body;
    var self = this;
    
    async.waterfall([
        function(getProfileCallback) {
            schemas.UserProfile.findOne(getProfileCallback);
        }, 
        function(profile, updateProfileCallback) {
            profile.name = body.profileName;
            profile.location = body.profileCity;
            profile.maxPrice = body.maxPrice;
            
            profile.save(updateProfileCallback);
        },
        function(savedProfile, count, getFlyCallback) {
            schemas.PreferencesSchema.findOne({
                    type: "fly",
                    service: "expedia"
                }, getFlyCallback);
        },
        function (flyProfile, saveFlyProfileCallback) {
            if (body.flyPlace == "Las Vegas") {
                flyProfile.location = "Las Vegas";
                flyProfile.airportCode = "LAS";
            } else {
                flyProfile.location = "Seattle";
                flyProfile.airportCode = "SEA";
            }
            
            flyProfile.save(saveFlyProfileCallback);
        },
        function(savedProfile, count, getDriveCallback) {
            schemas.PreferencesSchema.findOne({
                    type: "stay",
                    service: "expedia"
                }, getDriveCallback);
        },
        function (driveProfile, saveFlyProfileCallback) {
            if (body.drivePlace == "Portland") {
                driveProfile.geo = "45.512617,-122.678764";
                driveProfile.location = "Portland";
            } else {
                driveProfile.location = "Monterey";
                driveProfile.geo = "36.5930233,-121.8799647";
            }
                
            driveProfile.save(saveFlyProfileCallback);
        }
    ], function(err, res) {
        self.redirect("/#" + err);
    })
}

pagesController.userInfo = function () {
    var self = this;
    async.waterfall([
        function (getProfileCallback) {
            schemas.UserProfile.findOne(getProfileCallback);
        }
    ], function (err, result) {
        self.name = result.name;


        self.render();
    });

}

module.exports = pagesController;

