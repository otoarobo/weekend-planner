function removeBackgrounds() {
    var backgrounds = ["beachBackground", "sanFranciscoBackground", "sharksBackground", "roadtripBackground", "flyBackground"];

    for(var i = 0; i < backgrounds.length; i++) {
        $("body").removeClass(backgrounds[i]);
    }
    
    $(".cloud").hide();    
}

function setBackground(slide) {
    var loadedSlide = $(slide);
    
    removeBackgrounds();
    
    if(slide != null) {
        var sliderType = loadedSlide.find(".dayActivityContainerType").val();
        if(sliderType == "beach") {
            $("body").addClass("beachBackground");
        }
        else if (sliderType == "sharks") {
            $("body").addClass("sharksBackground");
        }
        else if (sliderType == "roadtrip") {
            $("body").addClass("roadtripBackground");
        }
        else if (sliderType == "fly") {
            $("body").addClass("flyBackground");
            $(".cloud").show();    
        }
        else
        {
            $("body").addClass("sanFranciscoBackground");
            $(".cloud").show();    
        }
    }
    else
    {
        $("body").addClass("sanFranciscoBackground");
        $(".cloud").show();    
    }
};

$(document).ready(function() 
{   
    $('#fullpage').fullpage
    (
        {
            // sectionsColor: [ 'rgba(255, 255, 255, .1)', '#4BBFC3', '#7BAABE', 'whitesmoke', '#000'],
            slidesColor: ['red', 'blue'],
            slidesNavigation: true,
            navigation: true,
            fitToSection: false,
            afterLoad: function() {
                setBackground(this.find(".slide, .active").first());
            },
            afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){                                    
                setBackground(this);
            }     
        }
    );    
});