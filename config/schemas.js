var mongoose = require("mongoose");

module.exports = {
    PreferencesSchema: mongoose.model('Preference', 
    { 
        type: String,
        location: String,
        username: String,
        importance: Number,
        zipCode: String,
        service: String,
        geo: String,
        minRating: Number,
        minImdbRating: Number,
        sport: String,
        team: String,
        maxLegs: Number,
        airportCode: String
    }),
    UserProfile: mongoose.model("UserProfile", 
    {
       name: String,
       location: String,
       zipCode: String,
       airportCode: String,
       maxPrice: Number,
    }),
    CacheSchema: mongoose.model("Cache", 
    {
        url: String,
        data: String
    })
};