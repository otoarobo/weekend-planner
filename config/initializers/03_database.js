var mongoose = require("mongoose");
var schemas = require("../schemas");
var async = require("async");

module.exports = function() {
    var url;
    
  switch (this.env) {
    case 'development':
        url = 'mongodb://localhost';
    
      break;
    case 'production':
      url = 'mongodb://weekendplanner:planner-123@ds011168.mlab.com:11168/weekend-planner';
      break;
  }
  
  console.log("DB env:" + url);
  
  async.waterfall([
        function(connectCallback) {
            mongoose.connect(url, connectCallback);
        },
        function(dropCallback) {
            console.log("Connected, dropping ...");
            
            async.eachSeries([schemas.PreferencesSchema, schemas.UserProfile], function(model, cb) {
                model.remove(cb);
            }, dropCallback);
        },
        function(insertPreferenceCallback) {
            var models = [];
            
            models.push(new schemas.PreferencesSchema({
                type: "place",
                location: "Santa Cruz",
                zipCode: 94085,                
                importance: 10
            }));
            
            models.push(new schemas.PreferencesSchema({
                type: "audioStreaming",
                service: "songkick",
                username: "otociulis",
                importance: 8
            }));
            
            models.push(new schemas.PreferencesSchema({
                type: "stay",
                service: "expedia",
                geo: "36.5930233,-121.8799647",
                location: "Monterey",                
                minRating: 4,
                importance: 9
            }));
            
            models.push(new schemas.PreferencesSchema({
                type: "fly",
                service: "expedia",
                location: "Seattle",
                airportCode: "SEA",
                maxLegs: 2,
                importance: 9
            }));
            
            // models.push(new schemas.PreferencesSchema({
            //     type: "movie",
            //     service: "fandango",                    
            //     minImdbRating: 8,
            //     importance: 5
            // }));
            
            models.push(new schemas.PreferencesSchema({
                type: "game",
                sport: "hockey",
                team: "SJS"
            }));
            
            models.push(new schemas.UserProfile({
                name: "Emily",
                location: "San Francisco, CA",
                zipCode: "94105",
                maxPrice: 200,
                airportCode: "SFO"    
            }));
            
            console.log("Dropped, inserting ...");
            
            async.eachSeries(models, function(model, cb) {
                model.save(cb);
            }, function(err, res) {
                insertPreferenceCallback(err, res);
            });
        }
    ], function (err, result) {
        console.log("DB init result" +  err);
    });
 
//   mongoose.model('Post', schemas.PostSchema);
}